# -*- coding: utf-8 -*-
"""
__author__ = 'peter'
__mtime__ = '2019-05-08'
# Follow the master,become a master.
             ┏┓       ┏┓
            ┏┛┻━━━━━━━┛┻┓
            ┃    ☃      ┃
            ┃  ┳┛   ┗┳  ┃
            ┃     ┻     ┃
            ┗━┓       ┏━┛
              ┃       ┗━━━━┓
              ┃ 神兽保佑     ┣┓
              ┃　永无BUG！   ┏┛
              ┗┓┓┏━━━┳┓┏━━━┛
               ┃┫┫   ┃┫┫
               ┗┻┛   ┗┻┛
"""
from rest_framework import serializers

from .models import Publisher, Book


# 类名固定为表名称 + Serializer
class PublisherSerializer(serializers.Serializer):
    # read_only必须为True,因为我们模型里面的id是一个自增字段,不可写,自动生成
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(max_length=32)
    address = serializers.CharField(max_length=128)

    def create(self, validated_data):
        # validated_data参数不需要特意去记,就是经过校验的数据
        return Publisher.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.address = validated_data.get('address', instance.address)
        instance.save()
        return instance


class PublisherSerializer2(serializers.ModelSerializer):
    """
    直接自定义序列化器序列化字段
    """
    operator_name = serializers.ReadOnlyField(source='operator.username')  # 重写序列化

    class Meta:
        model = Publisher  # 我们要使用的模型
        # 我们要使用的字段
        fields = (
            'id',
            'name',
            'address',
            'operator',  # 默认为外键值，则为关联表主键id
            'operator_name'  # 重写后的值，则为指定序列化字段
        )

    def create(self, validated_data):
        # validated_data参数不需要特意去记,就是经过校验的数据
        return Publisher.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.address = validated_data.get('address', instance.address)
        instance.save()
        return instance


class BookSerializer(serializers.ModelSerializer):
    """
    书的序列化器
    """
    publisher_name = serializers.StringRelatedField(source='publisher.name')

    class Meta:
        model = Book
        fields = (
            'id',
            'title',
            'publisher',
            'publisher_name'
        )


class BookSerializer2(serializers.HyperlinkedModelSerializer):
    """
    书的序列化器
    """
    publisher_name = serializers.StringRelatedField(source='publisher.name')

    class Meta:
        model = Book
        fields = (
            'id',
            'title',
            'publisher',
            'publisher_name'
        )
