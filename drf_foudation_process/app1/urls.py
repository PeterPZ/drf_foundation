# -*- coding: utf-8 -*-
"""
__author__ = 'peter'
__mtime__ = '2019-05-08'
# Follow the master,become a master.
             ┏┓       ┏┓
            ┏┛┻━━━━━━━┛┻┓
            ┃    ☃      ┃
            ┃  ┳┛   ┗┳  ┃
            ┃     ┻     ┃
            ┗━┓       ┏━┛
              ┃       ┗━━━━┓
              ┃ 神兽保佑     ┣┓
              ┃　永无BUG！   ┏┛
              ┗┓┓┏━━━┳┓┏━━━┛
               ┃┫┫   ┃┫┫
               ┗┻┛   ┗┻┛
"""
from django.urls import path

from .views import publisher_list, publisher_list2, publisher_list3, create_publisher, publisher_list4, publisher_list5, \
    publisher_detail, PublisherList, PublisherDetail, PublisherList2, PublisherDetail2

app_name = 'collection'

urlpatterns = [
    path('list', publisher_list, name='publisher_list'),
    path('list2', publisher_list2, name='publisher_list2'),
    path('list3', publisher_list3, name='publisher_list3'),
    path('list4', publisher_list4, name='publisher_list4'),
    path('create', create_publisher, name='create_publisher'),
    path('list5', publisher_list5, name='publisher_list5'),
    path('detail/<int:pk>', publisher_detail, name='publisher_detail'),
    path('CBV/list', PublisherList.as_view(), name='CBV_list'),
    path('CBV/detail/<int:pk>', PublisherDetail.as_view(), name='CBV_detail'),
    path('Mixin/list', PublisherList2.as_view(), name='Mixin_list'),
    path('Mixin/detail/<int:pk>', PublisherDetail2.as_view(), name='Mixin_detail'),
]
