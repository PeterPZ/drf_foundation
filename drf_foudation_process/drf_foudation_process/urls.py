"""drf_foudation_process URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.contrib import admin
from django.urls import path

from app1.views import BookList, BookDetail, BookList2, publisher_detail

urlpatterns = [
    path('admin/', admin.site.urls),
    path('publisher/', include('app1.urls', namespace='publisher')),
    path('detail/<int:pk>', publisher_detail, name='publisher-detail'),  # 指定命名空间返回空间地址url
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),  # 路由相关所有django-rest-framework的所有路由
    path('books/', BookList.as_view(), name='book_list'),
    path('books/<int:pk>', BookDetail.as_view(), name='book-detail'),
    path('books2/', BookList2.as_view(), name='book-list'),
]
