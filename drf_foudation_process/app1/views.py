# Create your views here.
import json

from django.core import serializers
from django.http import Http404
from django.http import HttpResponse
from rest_framework import generics
from rest_framework import mixins
from rest_framework import permissions
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Publisher, Book
from .permissions import IsOwnerOrReadOnly
from .serializers import PublisherSerializer, PublisherSerializer2, BookSerializer, BookSerializer2


def publisher_list(request):
    # 查询出所有的出版社
    queryset = Publisher.objects.all()
    # 转换成python中的列表
    data = []
    for i in queryset:
        # 每一个对象都手动转化成一个字典
        p_tmp = {
            'name': i.name,
            'address': i.address
        }
        data.append(p_tmp)

    return HttpResponse(json.dumps(data), content_type='application/json')


def publisher_list2(request):
    # 查询出所有的出版社
    queryset = Publisher.objects.all()
    # 直接用现成的序列化器
    data = serializers.serialize('json', queryset)
    return HttpResponse(data, content_type='application/json')


def publisher_list3(request):
    p1 = Publisher.objects.first()  # 先找到一个出版社的对象
    s = PublisherSerializer(p1)
    print(s.data)
    print(type(s.data))
    return HttpResponse(json.dumps(s.data), content_type='application/json')


def publisher_list4(request):
    """
    自定义序列化器实现列表
    :param request:
    :return:
    """
    queryset = Publisher.objects.all()
    s = PublisherSerializer2(queryset, many=True)
    return HttpResponse(json.dumps(s.data), content_type='application/json')


def create_publisher(request):
    """
    创建新对象
    :param request:
    :return:
    """
    p2 = {'name': '图灵出版社', 'address': '大兴天宫院'}
    s = PublisherSerializer(data=p2)
    s.is_valid()  # 如果数据检测没有问题
    s.validated_data  # 可以查看类型,观察到这是一个有序字典
    s.save()  # 保存到数据库
    return HttpResponse(json.dumps(s.data), content_type='application/json')


# 列表里面的参数是被允许的操作,比如只有GET/POST请求,如果不是get或者post会报405－－－>405 Method Not Allowed
@api_view(['GET', 'POST'])
def publisher_list5(request):
    """
    列出所有出版社,get
    或者创建一个新的出版社 post
    """
    if request.method == 'GET':
        # 所有的出版社
        queryset = Publisher.objects.all()
        # 把所有从数据库取出来的出版社信息数据进行序列化
        s = PublisherSerializer(queryset, many=True)
        return Response(s.data)
    if request.method == 'POST':
        # post请求就是---->创建出版社
        # 跟上面不同,从数据转化成序列化的参数
        # 说白了就是把客户端传过来的数据,用序列化生成实例对象
        s = PublisherSerializer2(data=request.data)
        if s.is_valid():  # 如果数据没问题
            s.save()
            return Response(s.data, status=status.HTTP_201_CREATED)
        else:
            return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)


# GET 获取出版社　
@api_view(['GET', 'PUT', 'DELETE'])
def publisher_detail(request, pk):
    try:
        # 从数据库里面找你要找的pk
        publisher = Publisher.objects.get(pk=pk)
    except Publisher.DoesNotExist:  # 如果找不到浏览器传来的pk对应的数据,返回４０４
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        # 从数据库里面取出来的publisher进行序列化
        s = PublisherSerializer(publisher)
        return Response(s.data)

    if request.method == 'PUT':  # PUT请求直接调用序列化器中update方法
        # publisher使我们查出来的出版社信息　　request.data是客户端传过来的
        s = PublisherSerializer(publisher, data=request.data)
        if s.is_valid():  # 如果数据没有问题
            s.save()
            return Response(s.data)
        else:
            return Response(status=status.HTTP_204_NO_CONTENT)

    if request.method == 'DELETE':  # DELETE请求直接将对象进行删除
        publisher.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class PublisherList(APIView):
    """
    列出所有的出版,get
    或者创建一个新的出版社post
    """

    def get(self, request, format=None):
        queryset = Publisher.objects.all()  # 查询出所有出版社
        s = PublisherSerializer(queryset, many=True)
        return Response(s.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        s = PublisherSerializer(data=request.data)
        if s.is_valid():
            s.save()
            return Response(s.data, status=status.HTTP_201_CREATED)
        else:
            return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)


class PublisherDetail(APIView):
    """
    具体的某一个出版社　　查看．修改．删除的视图
    """

    # 需要先尝试的从数据库查到　pk对应的数据,如果没有返回404
    def get_object(self, pk):
        try:
            return Publisher.objects.get(pk=pk)
        except Publisher.DoesNotExist:
            raise Http404  # 需要先导入　from django.http import Http404

    def get(self, request, pk, format=None):
        publisher = self.get_object(pk)
        s = PublisherSerializer2(publisher)
        return Response(s.data, status=status.HTTP_200_OK)

    def put(self, request, pk, format=None):
        publisher = self.get_object(pk)
        s = PublisherSerializer2(publisher, data=request.data)
        if s.is_valid():
            s.save()
            return Response(s.data)
        else:
            Response(s.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        """删除出版社信息"""
        publisher = self.get_object(pk)
        publisher.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class PublisherList2(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):
    """
    利用Mixin列出所有的出版,get
    或者创建一个新的出版社post
    """
    serializer_class = PublisherSerializer2
    queryset = Publisher.objects.all()
    permission_classes = (permissions.IsAuthenticated,)  # 为单独api添加认证用户权限

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class PublisherDetail2(mixins.RetrieveModelMixin,
                       mixins.UpdateModelMixin,
                       mixins.DestroyModelMixin,
                       generics.GenericAPIView):
    """
    利用Mixin具体的某一个出版社　　查看．修改．删除的视图
    """
    queryset = Publisher.objects.all()
    serializer_class = PublisherSerializer2
    permission_classes = (IsOwnerOrReadOnly,)  # 为单独api添加自定义权限限制，为当前创建者可改

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class BookList(generics.ListCreateAPIView):
    """
    书籍列表视图
    """
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = (permissions.IsAuthenticated,)


class BookList2(generics.ListCreateAPIView):
    """
    书籍列表视图
    """
    queryset = Book.objects.all()
    serializer_class = BookSerializer2
    permission_classes = (permissions.IsAuthenticated,)


class BookDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    书籍详情视图
    """
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = (permissions.IsAuthenticated,)
